package io.catalyte.training.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static io.catalyte.training.constants.StringConstants.CONTEXT_PETS;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@SpringBootTest
@AutoConfigureMockMvc
class PetControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();

    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;


    @BeforeEach
    public void setUp(){
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Test
    public void getPetThatDoesExistById() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS + "/1"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Cletus")));
    }


    @Test
    public void queryPetsReturnsThree() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(3))));
    }


    @Test
    public void saveAllPets() throws Exception {
        List<String> json = new ArrayList<>();
        json.add("{\"name\":\"Murphy\",\"breed\":\"tuxedo\",\"age\":2}");
        json.add("{\"name\":\"Doudou\",\"breed\":\"tuxedo\",\"age\":3}");

        this.mockMvc
                .perform(post(CONTEXT_PETS + "/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(json)))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", isA(ArrayList.class)))
                .andExpect(jsonPath("$", hasSize(2)));
    }


    @Test
    public void saveNewPet() throws Exception {
        String json = "{\"name\":\"Murphy\",\"breed\":\"tuxedo\",\"age\":2}";

        this.mockMvc
                .perform(post(CONTEXT_PETS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Murphy")));
    }


    @Test
    public void updatePetById() throws Exception {
        String json = "{\"id\":1,\"name\":\"Cletus\",\"breed\":\"cat\",\"age\":6}";

        this.mockMvc
                .perform(put(CONTEXT_PETS + "/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.breed", is("cat")));
    }


    //@DirtiesContext
    @Test
    public void deletePet() throws Exception {
        mockMvc
                .perform(delete(CONTEXT_PETS + "/4"))
                .andExpect(deletedStatus);
    }

}